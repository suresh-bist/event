class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :masqueradable, :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :omniauthable

  has_person_name

  has_many :notifications, foreign_key: :recipient_id
  has_many :services

  has_many :created_events, :foreign_key => "creator_id", :class_name => "Event"
  has_many :event_attendees, :foreign_key => "attendee_id", :class_name => "EventAttendee"
  has_many :attended_events, :through => :event_attendees, source: "attended_event"

	validates :name, presence: true, length: { minimum: 4 }
end
