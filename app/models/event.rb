class Event < ApplicationRecord
	belongs_to :creator, :class_name => "User"
	has_many :event_attendees, :foreign_key => "attended_event_id", :class_name => "EventAttendee"
	has_many :attendees, :through => :event_attendees, source: "attendee"
	
	scope :upcoming, lambda { where("#{:date} >= ?" , Date.current) }
	scope :past, lambda { where("#{:date} < ?" , Date.current) }

	default_scope -> { order(:date) }
	
	validates :title, presence: true, length: { maximum: 25 }
	validates :date, presence: true
	validate :is_event_date_past

	def is_event_date_past
	  errors.add(:date, "can't be in the past") if is_past_event?
	end

	def is_past_event?
		date < Date.current
	end

end

