# README

## Events Website
The goal is to create a simple ticketing platform which displays a bunch of events. Users can register or login. They can visit an event page, attend an event and see who else is attending.

### Ruby
* Object oriented, dynamically typed and expressive syntax
* Single threaded execution model. Run with multi-process configuration to achieve concurrent behavior
* Large eco-system of libraries and frameworks
* Provides rapid-application development capabilities

### Ruby On Rails
* A web-application framework that includes everything needed to create database-backed web applications according to the Model-View-Controller (MVC)

### PostgresSql
* RDBMS, Modern, high-performance, ACID compliant
* This is our primary data-store for all customer data
* Supports modern facilities like CTEs, programmability
* Supports high-availability features like replications, backups, clustering capabilities
* Used primarily via Amazon Relational Database Service (RDS)

### Template Used
* [Jumpstart](https://github.com/excid3/jumpstart) - Jumpstart is a Rails template, so you pass it in as an option when creating a new app. Refer this to start the app.

### Installation for macOS
* run script `setup.sh`
(Haven't tested the script)

## Approach
* Entities - User, Event
* Join Table is introduced EventAttendees to get the User attending events and User created events